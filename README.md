# Quilmes_SLiM_Course_2018

## Friday day 4 Schedule (modifiable by G20…)

|               | Session |
|---------------|---------|
| 9:30 - 10:00  | Install Jalview |
| 10:00 - 11:00 | Introduction to short linear motifs and ELM |
| 11:00 - 11:30 | Maté break |
| 11:30 - 13:00 | [Practical with ELM and other motif tools][elm] (e.g. Pecan, ProViz, SLiMSearch, Phosphosite etc.) |
| 13:00 - 14:00 | Lunch |
| 14:00 - 15:00 | Presentation: Modular protein architecture and Complexity of Cell Regulation |
| 15:00 - 15:30 | Maté break |
| 15:30 - 17:00 | [Multiple alignments of modular proteins with Jalview][jalview] and [viewing SLiM structures in Chimera][chimera] |

[jalview]: https://git.embl.de/gouw/quilmes_slim_course_2018/raw/master/MSA_JalView_Exercise-2018.pdf
[chimera]: https://git.embl.de/gouw/quilmes_slim_course_2018/raw/master/Chimera-Structure-2018.pdf
[elm]: https://git.embl.de/gouw/quilmes_slim_course_2018/raw/master/SLiM-Resource-Exercises.pdf

